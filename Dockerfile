FROM node:14-alpine

RUN apk update && apk add --no-cache git
RUN apk add --virtual build-dependencies build-base gcc wget
RUN apk add python2
RUN yarn config set cache-folder .yarn